﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Challenge1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string[] splitWords = SplitUserString(MainTextBox.Text);
            PrintSplitWords(splitWords);
        }

        private string[] SplitUserString(string str)
        {
            return str.Split(" ");
        }

        private void PrintSplitWords(string[] words)
        {
            FirstListBox.Items.Clear();
            foreach (var e in words)
            {
                FirstListBox.Items.Add(e);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string reverseString = ReverseString(MainTextBox.Text);
            SecondLabel.Content = reverseString;
        }

        private string ReverseString(string str)
        {
            string[] splitWords = SplitUserString(str);
            string[] reverseSplitWords = new string[splitWords.Length];
            for (int i = 0; i < splitWords.Length; i++)
            {
                reverseSplitWords[splitWords.Length - i - 1] = splitWords[i];
            }
            string reverseString = string.Join(" ", reverseSplitWords);
            return reverseString;
        }
    }
}
